
#include <iostream>
#include <fstream>
#include <cstring>
#include <RbtCompression.hpp>
#include <bundle.h>
#include <bundle.cpp>

#define eprint(err) std::cerr << err;
#define eprintln(err) std::cerr << err << std::endl;

bool is_ascii(const signed char *c, size_t len) {
  for (size_t i = 0; i < len; i++) {
    if (c[i] < 0) return false;
  }
  return true;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
// Converts char[8] (little endian) to uint64_t

uint64_t LE_Bytes::from(const char* bytes) {
    uint64_t u64;
    memcpy(&u64, bytes, sizeof u64);
    return le64toh(u64);
}

// Convert uint64_t to char[8] (little-endian)
void LE_Bytes::to(uint64_t uint64, char* destBuf) {
    uint64_t u64 = htole64(uint64);
    memcpy(destBuf, &u64, sizeof u64);
}
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

// Read compressed index from inputFile and store in compressedIndex
bool CmZ::archive::readCompressedIndex() {
    // Save position in file
    uint64_t currentPos = inputFile.tellg();

    try {
        // Find length of file
        inputFile.seekg(0, std::ios_base::end);
        uint64_t fileSize = inputFile.tellg();

        // Read index length
        inputFile.seekg(fileSize - 8);
        char* indexSizeRaw = new char[8];
        inputFile.read(indexSizeRaw, 8);
        
        uint64_t indexSize = LE_Bytes::from(indexSizeRaw);
        
        // Read data into compressedIndex
        inputFile.seekg(fileSize - indexSize - 8);
        compressedIndex.resize(indexSize);
        inputFile.read(&compressedIndex[0], indexSize);
        
        // Seek to saved position
        inputFile.seekg(currentPos);

        return true;
    } catch (const std::exception &exc) {
        inputFile.seekg(currentPos);
        eprintln(exc.what());
        return false;
    }
}

// Decompress data in compressedIndex, parse, then store it in index
bool CmZ::archive::decompressIndex() {
    try {
        std::vector<char> indexRaw;
        indexRaw = bundle::unpack(compressedIndex); // Decompress to get size
        index.resize(indexRaw.size() / 8);
        
        char* buffer = new char[8];
        int i = 0;
        int j = 0;
        for (char byte : indexRaw) {
            buffer[i] = byte;
            i ++;
            if (i == 8) {
                index[j] = LE_Bytes::from(buffer);
                j ++;
                i = 0;
            }
        }

        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }
}

// Convert index to std::vector<char> (little endian) then compress into compressedIndex
bool CmZ::archive::compressIndex() {
    try {
        uint64_t i = 0;
        std::vector<char> indexRaw;
        indexRaw.resize(index.size() * 8); // Allocate memory
        for (uint64_t ind : index) { 
            char* buffer = new char[8];
            LE_Bytes::to(ind, buffer); // Convert uint64_t to char[8] 
            for (int j = 0; j < 8; j ++) {
                indexRaw[i] = buffer[j]; // Append data to temp index
                i ++;
            }
        }
        compressedIndex = bundle::pack(mode, indexRaw); // Compress index
        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }
}

uint64_t CmZ::archive::calculateOffset(uint64_t ind) {
    uint64_t offset = 0;
    if (ind == UINT64_MAX) {
        ind = index.size() - 1;
    }

    for (size_t i = 0; i <= ind; i ++) {
        offset += index[i];
    }

    return offset;
}

// Clear compressedRecord and read new compressed record data from file 
bool CmZ::archive::readRecordAtIndex(uint64_t ind) {
    if (ind == UINT64_MAX) {
        ind = index.size() - 2;
    }
    try {
        // Calculate record's offset from index
        uint64_t offset = calculateOffset(ind);
        uint64_t size = index[ind + 1]; // Read record's size from index

        // Read data into compressedRecord
        inputFile.seekg(offset);
        compressedRecord.resize(size);
        inputFile.read(&compressedRecord[0], size);

        nextRecord = ind + 1;

        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }

}

bool CmZ::archive::readNextRecord() {
    try {
        readRecordAtIndex(nextRecord);
        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }
}

// Compress decompressedRecord and store in compressedRecord
bool CmZ::archive::compress() {
    try {
        compressedRecord = bundle::pack(mode, decompressedRecord);
        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }
}

// Decompress compressedRecord and store in decompressedRecord
bool CmZ::archive::decompress() {
    try {
        decompressedRecord = bundle::unpack(compressedRecord);
        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }
}

// Calculate index entry and append to index
bool CmZ::archive::addCurrentToIndex() {
    try {
        if (index.size() == 0) 
            index.push_back(0);
        index.push_back(compressedRecord.size());
        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }
}

// Write to file from appropriate buffer
bool CmZ::archive::writeRecordToFile() {
    try {
        if (mode == DECOMPRESS) {
            outputFile.write((char*) decompressedRecord.data(), decompressedRecord.size());
        } else {
            outputFile.write((char*) compressedRecord.data(), compressedRecord.size());
        }
        outputFile.sync();
        return true;
    } catch (const std::exception &exc) {
        eprintln(exc.what());
        return false;
    }
}

std::vector<uint64_t> CmZ::rebuildIndex(std::string strInputFile) {
    std::vector<uint64_t> index = {};
    std::ifstream input(strInputFile, std::ios_base::binary);
    uint64_t pos = 0;
    uint64_t recordStart = 0;
    char *header = new char[32];
    unsigned iAlgorithm;
    while (input) {
        char currentByteBuf[5];
        input.seekg(pos);
        input.read(currentByteBuf, 5);
        if (currentByteBuf[0] == 0x00 && currentByteBuf[1] == 0x00 && currentByteBuf[2] == 0x00) {
            if (currentByteBuf[3] == 0x70) {
                if ((unsigned int)currentByteBuf[4] < 24) { // Found header
                    if (index.size() == 0)
                        index.push_back(recordStart);
                    input.seekg(recordStart);
                    input.read(header, 32);
                    size_t pad = bundle::padding(header, 32);
                    iAlgorithm = header[pad + 1];
                    const char *ptr = (const char *)&header[pad + 2];
                    size_t unpackedSize = bundle::vlebit(ptr);
                    size_t packedSize = bundle::vlebit(ptr);
                    index.push_back(packedSize + 32);
                    pos = recordStart + packedSize + 32;
                    recordStart = pos;
                    continue;
                }
            }
        }
        pos++;
        if (currentByteBuf[0] != 0x00) {
            recordStart = pos;
        }
    }
    input.close();
    return index;
}

#ifdef _WIN32
std::vector<uint64_t> CmZ::rebuildIndex(std::string strInputFile, indicators::ProgressBar &progressBar) {
#else
std::vector<uint64_t> CmZ::rebuildIndex(std::string strInputFile, indicators::BlockProgressBar &progressBar) {
#endif
    std::vector<uint64_t> index = {};
    std::ifstream input(strInputFile, std::ios_base::binary);
    input.seekg(0, std::ios_base::end);
    uint64_t size = input.tellg();
    input.seekg(0, std::ios_base::beg);
    uint64_t pos = 0;
    uint64_t recordStart = 0;
    char *header = new char[32];
    unsigned iAlgorithm;
    while (input) {
        char currentByteBuf[5];
        input.seekg(pos);
        input.read(currentByteBuf, 5);
        if (currentByteBuf[0] == 0x00 && currentByteBuf[1] == 0x00 && currentByteBuf[2] == 0x00) {
            if (currentByteBuf[3] == 0x70) {
                if ((unsigned int)currentByteBuf[4] < 24) { // Found header
                    if (index.size() == 0)
                        index.push_back(recordStart);
                    input.seekg(recordStart);
                    input.read(header, 32);
                    size_t pad = bundle::padding(header, 32);
                    iAlgorithm = header[pad + 1];
                    const char *ptr = (const char *)&header[pad + 2];
                    size_t unpackedSize = bundle::vlebit(ptr);
                    size_t packedSize = bundle::vlebit(ptr);
                    index.push_back(packedSize + 32);
                    pos = recordStart + packedSize + 32;
                    recordStart = pos;
                    double progress = static_cast<double>(pos) / static_cast<double>(size);
                    progressBar.set_option(indicators::option::PostfixText{"Headers found: " + std::to_string(index.size() - 2)});
                    progressBar.set_progress(progress * 100);
                    continue;
                }
            }
        }
        pos++;
        if (currentByteBuf[0] != 0x00) {
            recordStart = pos;
        }
        double progress = static_cast<double>(pos) / static_cast<double>(size);
        progressBar.set_progress(progress * 100);
    }
    input.close();
    return index;
}

bool CmZ::isValidMdlHeader(std::vector<char> input) {
    std::vector<char>::iterator offset = input.begin();
    std::uint64_t readLen = input.size() - 1;

    // Header should have at least 3 lines
    if (std::count(offset, offset + readLen, '\n') < 3) {
        return false;
    }

    // Find location of 3rd new line
    for (int i = 0 ; i < 3; i ++) {
        offset = std::find(offset, offset + readLen, '\n');
        readLen = readLen - std::distance(input.begin(), offset);
    }

    int size = std::distance(input.begin(), offset);
    signed char *header = new signed char[size];
    std::memmove(header, input.data(), size);

    if (is_ascii(header, size)) {
        return true;
    }

    return false;
}

void CmZ::printSupportedAlgorithms() {
    std::cout << "Supported compression algorithms:";
    int i = 0;
    for (unsigned int encoder : bundle::allEncoders()) {
        if (i % 4 == 0)
            std::cout << std::endl << " ";
        else
            std::cout << "\t";
        if (i < 10) 
            std::cout << " ";
        std::cout << i << " - " << encoderName(encoder);
        i++;
    }
    std::cout << std::endl;
}

std::string CmZ::encoderName(unsigned iAlgorithm) {
    return bundle::encoderNames[iAlgorithm];
}

unsigned CmZ::encoder(unsigned iAlgorithm) {
    return bundle::getEncoder(iAlgorithm);
}

bool CmZ::isAlgorithmSupported(unsigned iAlgorithm) {
    if (std::binary_search(std::begin(bundle::unsupported), std::end(bundle::unsupported), iAlgorithm))
        return false;
    return true;
}

int CmZ::findAlgorithm(std::vector<char> bytes) {
    for (int i = 0; i < bytes.size() - 3; i ++) {
        if (bytes[i] == (char)0x00 && bytes[i+1] == (char)0x00 && bytes[i+2] == (char)0x00 && bytes[i+3] == (char)0x70){
            if ((unsigned int)bytes[i+4] < 24)
                return (int)bytes[i + 4];
        }
    }
    return INT_MAX;
}

bool CmZ::FileIsEmpty(std::string strFileName) {
    std::fstream file(strFileName,
        std::ios_base::in | std::ios_base::out | std::ios_base::binary);
    file.seekp(0, std::ios_base::end);
    uint64_t size = file.tellp();
    file.close();
    return size < 8;
}